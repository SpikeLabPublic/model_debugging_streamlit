import matplotlib.pyplot as plt
import streamlit as st
import pandas as pd
import numpy as np
import shap
import time
import h2o

def main():
    ipA = "127.0.0.1"
    portN = "54321"
    urlS = "http://127.0.0.1:54321"

    connect_type=h2o.connect(ip=ipA, port=portN, verbose=True)

    
    st.title('Model Debugging on h2o and streamlit')
    st.sidebar.header('Así usamos la sidebar')
    
    #select box
    lista_opciones = ['Mirar dataset',
                      'Métricas y variables importantes',
                      'Partial dependence plots',
                      'SHAP',
                      'Fairness']
    menu = st.sidebar.selectbox('Escogemos qué mirar del modelo',
                        lista_opciones, key='menu')

    if menu==lista_opciones[0]:
        dataset()
    if menu==lista_opciones[1]:
        metricas_modelo()
    elif menu==lista_opciones[2]:
        pdps()
    elif menu==lista_opciones[3]:
        shaps()
    elif menu==lista_opciones[4]:
        fairnes()
    

def dataset():
    train, test = cargar_datos()
    st.subheader('Header de la tabla')
    df_test = test.as_data_frame()
    st.table(df_test.head())
    #profile = ProfileReport(df_test[:1000], title='Pandas Profiling Report')
    #st.write(profile)
    
    
    
def metricas_modelo():
    #Cargamos modelos
    modelo = cargar_modelo()

    st.header('Cross validation metrics summary')
    tabla_metrics = modelo._model_json['output']['cross_validation_metrics_summary'].as_data_frame()
    st.table(tabla_metrics[[m for m in tabla_metrics.columns if 'cv_' not in m ]])
    
    st.header('Importancia de variables')
    st.pyplot(modelo.varimp_plot())
    
    st.header('Gains lift table on validation frame')
    gainslift = modelo.gains_lift(valid=True).as_data_frame()
    gainslift.set_index('group', inplace=True)
    st.table(gainslift[[m for m in gainslift.columns if 'cum' in m and 'score' not in m]].head())

def pdps():
    st.header('Partial dependence plot')
    st.subheader('Partial dependence plot gives a graphical depiction of the marginal effect of a variable on the response. The effect of a variable is measured in change in the mean response')
    # Cargar modelo y datos
    modelo = cargar_modelo()
    train, test = cargar_datos()
    var_to_dpdplot = st.sidebar.selectbox('Escogemos qué variable queremos analizar',
                        train.columns, key='var_pdp')
    
    graficar_pdp(modelo, var_to_dpdplot, test)
        

def shaps():
    st.header('SHapley Additive exPlanations (SHAP)')
    st.subheader('SHAP is a game theoretic approach to explain the output of any machine learning model. It connects optimal credit allocation with local explanations using the classic Shapley values from game theory and their related extensions')
    
    # Cargar modelo y datos
    modelo = cargar_modelo()
    train, hf = cargar_datos()
    contributions = modelo.predict_contributions(hf).as_data_frame()

    # shap values are calculated for all features
    cols_contrib = [m for m in contributions.columns if 'Bias' not in m]
    shap_values = contributions[cols_contrib].as_matrix()

    # expected values is the last returned column
    expected_value = contributions['BiasTerm'].min()
    
    shap.initjs()
    st.subheader('Miramos el comportamiento global del modelo')
    test = hf.as_data_frame()

    
    shap.summary_plot(shap_values, test[cols_contrib])
    st.pyplot()
    
    st.subheader('Miramos una instancia en particular')
    
    N = hf.shape[0]
    k = st.slider('Qué instancia queremos mirar?', 0, N, 25)
    shap.force_plot(expected_value, shap_values[k,:], test.loc[k,cols_contrib], link='logit', matplotlib=True,
                   figsize=(15, 5))
    st.pyplot()
   
    #SHAP GLOBAL
def fairnes():
    st.header('Fairnesss')
    st.subheader('In machine learning, a given algorithm is said to be fair, or to have fairness if its results are independent of some variables we consider to be sensitive and not related with it (f.e.: gender, ethnicity, sexual orientation, etc.).')
    # Cargar modelo y datos
    modelo = cargar_modelo()
    train, hf = cargar_datos()
    cols_to_fairness = ['race','gender']
    var_sensitive = st.selectbox('Escogemos qué variable queremos analizar',
                        cols_to_fairness, key='var_fairness')
    
    
    
    predicted = hf.cbind(modelo.predict(hf)).as_data_frame()
    
    st.subheader('Miramos el promedio agrupado por variable')
    dati = pd.DataFrame(predicted.groupby(var_sensitive)['p1'].mean()).rename(columns={'p1':'prob'}).T
    st.table(dati)
    
    with plt.xkcd():
        fig, ax = plt.subplots(1,1, figsize=(10,8))
        for kind in predicted[var_sensitive].unique():
            predicted[predicted[var_sensitive]==kind]['p1'].plot.kde(label=kind, legend=True, ax=ax)

        ax.set_xlabel('probabilidad')
        ax.set_title('Densidad abriendo por la variable {var}'.format(var=var_sensitive))
    st.pyplot()
    
    

def graficar_pdp(model, var, hf):

    pdp = model.partial_plot(hf,[var], plot=False)
    df_ = pdp[0].as_data_frame()
    with plt.xkcd():
        fig, ax = plt.subplots(1, 1, figsize=(10, 8))
        df_.plot(x=var, y='mean_response', rot=30, ax=ax)
        df_['limsup'] = df_['mean_response']+df_['stddev_response']
        df_['liminf'] = df_['mean_response']-df_['stddev_response']
        df_.plot(x=var, y='limsup',ax=ax, legend=False, marker=0, color='salmon',rot=30)
        df_.plot(x=var, y='liminf',ax=ax, legend=False, marker=0, color='salmon',rot=30)
        ax.set_title('pdp of {var}'.format(var=var))
        st.pyplot()
    

@st.cache 
def cargar_modelo():
    modelo = h2o.get_model(model_id="GBM_model_python_1583439964029_1")
    return modelo


#@st.cache
def cargar_datos():
    train = h2o.get_frame("train")
    test = h2o.get_frame("test")
    return train, test
    


main()