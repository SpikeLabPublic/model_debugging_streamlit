# model_debugging_streamlit

Se desarrolla un panel con streamlit que busca debuggear modelos de h2o.

Primero, se entrena un modelo de h2o en un notebook, para luego cargarlo y tener una aplicación con streamlit que busca debuggear un modelo de h2o.

- Streamlit: https://www.streamlit.io/
- Documentación: https://docs.streamlit.io/api.html
- Ejemplos y galería: http://awesome-streamlit.org/

Para correr la app, luego de instalar streamlit (pip install streamlit), ir al directorio y correr
`streamlit run app.py`, lo que carga la app en el localhost:8501.

Se incluye en debugging:
- Fairness
- SHAP
- PDP
- importancia de variables